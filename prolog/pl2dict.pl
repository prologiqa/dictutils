:-module(pl2dict,[pl2dict/2, pl2dict/3, pl2dict/4
                 ,dict2pl/2, dict2pl/3, dict2pl/4]).

/** <module> - Prolog expression to dict transformation and back

 The module exports predicates to transform Prolog expressions to
 dicts and back in a programmer controlled way...

 WARNING! pl2dict<-->dict2pl are not necessarily/always symmetric!!

 @author  Kili�n, Imre (Univ. P�cs, Hungary)
          kilian@gamma.ttk.pte.hu
 @license GPL

*/

:- use_module(library(apply)).
:- use_module(library(plUnit)).

%! pl2dict(+PLEXPR:expr, -DICT:dict) is det.

%! pl2dict(:GEO, +PLEXPR:expr, -DICT:dict) is det.

%! pl2dict(:MACROS, :GEO, +PLEXPR:expr, -DICT:dict) is det.
%
% @arg GEO     Feature geometry(syntax) a GEO(NAME:atom,KEYLIST:list)
%              predicate name to define Prolog2Prolog
% @arg MACROS  a binary predicate name to define
%              Prolog2Prolog transformation macros... Macros
%              are applied through call/2
% @arg PLEXPR  Prolog expression
% @arg DICT    Prolog dict
%
% The first part of the module implements a prolog to dict
% transformation As far as complete expressions are concerned:
% - variables remain variables
% - atomics remain atomics
% - lists become lists of transformed expressions
% - expressions become dicts with the name,
%   arguments are transformed to dict arguments, if possible
%
% Dict arguments are transformed a little differently:
% - atomics are transformed to themselfes
% - name:value constructions are transformed to dict arguments
%   provided that value can be transformed to a dict
% - name(value) constructions are transformed in the same way
% - name(val1,...valn) constructions are transformed to name:dict
%   provided val1,...,valn can be transformed to a dict
%
% Macros are applied first, but only once
% (no macro is applied to macro result again)
%
pl2dict(EXPR,DICT):- pl2dict(=,=,EXPR,DICT).

pl2dict(GEO,EXPR,DICT):- pl2dict(=,GEO,EXPR,DICT).

pl2dict(_,_,VAR,VAR):- var(VAR), !.
pl2dict(CALL,GEO,EXPR0,DICT):- call(CALL,EXPR0,EXPR), !,
    pl2Dict(CALL,GEO,EXPR,DICT).
pl2dict(CALL,GEO,EXPR,DICT):- pl2Dict(CALL,GEO,EXPR,DICT).

pl2Dict(_CALL,_GEO,ATOMIC,ATOMIC):- atomic(ATOMIC), !.
pl2Dict(CALL,GEO,LIST,DICTS):- is_list(LIST), !,
    maplist(pl2dict(CALL,GEO),LIST,DICTS).
%expressions for them a translation rule (key vector) is given
pl2Dict(CALL,GEO,EXPR,DICT):- nonvar(EXPR), EXPR =.. [NAME|LIST0],
    functor(EXPR,NAME,LN), call(GEO,NAME,KEYS),
    KEYS \= NAME, length(KEYS,LN), !,
    maplist(pl2dict(CALL,GEO),LIST0,LIST),
    maplist(key2ColonAttr,KEYS,LIST,ATTRS),
    dict_create(DICT,NAME,ATTRS).
%if there is no key vector than we are trying to define keys
%unary constructions remain so...
pl2Dict(CALL,GEO,EXPR,DICT):- nonvar(EXPR), EXPR =.. [NAME,LI|ST],
    maplist(pl2dictArg(CALL,GEO),LI|ST,DICTS), !,
    dict_create(DICT,NAME,DICTS).
pl2Dict(_,_,EXPR,EXPR).


key2ColonAttr(KEY,VALUE,KEY:VALUE).

key2DashAttr(KEY,VALUE,KEY-VALUE).

pl2dictArg(CALL,GEO,EXPR0,NAME-DICT):- call(CALL,EXPR0,EXPR), !,
    pl2DictArg(CALL,GEO,EXPR,NAME:DICT).
pl2dictArg(CALL,GEO,EXPR,NAME-DICT):-
    pl2DictArg(CALL,GEO,EXPR,NAME:DICT).

pl2DictArg(_CALL,_GEO,ATOM,ATOM:true):- atom(ATOM), !.
pl2DictArg(CALL,GEO,ATOMIC:EXPR,ATOMIC:DICT):-
    atomic(ATOMIC), !, pl2dict(CALL,GEO,EXPR,DICT).
pl2DictArg(CALL,GEO,LIST,list:DICT):- is_list(LIST), !,
    maplist(pl2dictArg(CALL,GEO),LIST,DICTS), dict_create(DICT,list,DICTS).
pl2DictArg(CALL,GEO,PAR1EXPR,NAME:DICT):-
    compound(PAR1EXPR), PAR1EXPR =.. [NAME,EXPR], !,
    pl2dict(CALL,GEO,EXPR,DICT).
pl2DictArg(CALL,GEO,EXPR,NAME:DICT):-
    compound(EXPR), EXPR =.. [NAME|EXPRS],
    maplist(pl2dictArg(CALL,GEO),EXPRS,DICTS),
    dict_create(DICT,_,DICTS).


%! dict2pl(+DICT:dict, -PLEXPR:expr) is det.

%! dict2pl(:GEO, +DICT:dict, -PLEXPR:expr) is det.

%! dict2pl(:MACROS, :GEO, +DICT:dict, -PLEXPR:expr) is det.
%
% @arg GEO     Feature geometry(syntax) a GEO(NAME:atom,KEYLIST:list)
%              predicate name to define Prolog2Prolog
% @arg MACROS  a binary predicate name to define
%              Prolog2Prolog transformation macros... Macros
%              are applied through call/2
% @arg DICT    Prolog dict
% @arg PLEXPR  Prolog expression
%
% @error error(type_error(dict,DICT),_) is thrown
%        when DICT is not a Prolog dictionary
%
% The second half of the module implements a dict to Prolog
% transformation solution. The point is to transform a thick dict to a
% sparse Prolog expresson. To do this a dict-geometry is defined in
% DICT(NAME,LIST) where LIST is a list of dict attribute selectors...
% those define the order of arguments in the Prolog expression Thus,
% having noticed a NAME dict tag, an expression of NAME/length(LIST) is
% generated. If there are no matching DICT/2 clauses, then the generated
% structure corresponds to the actual attribute list, only the attribute
% selectors are thrown away. Macros are applied last, but only once (no
% macro is applied to macro result again)
%

dict2pl(DICT,EXPR):-
    \+ is_dict(DICT)-> must_be(dict,DICT);
    dict2Pl(=,=,DICT,EXPR).

dict2pl(GEO,DICT,EXPR):-
    dict2Pl(=,GEO,DICT,EXPR).

dict2pl(CALL,GEO,DICT,EXPR):-
    dict2Pl(CALL,GEO,DICT,EXPR0),
    (call(CALL,EXPR0,EXPR), !; EXPR=EXPR0).
dict2Pl(CALL,GEO,DICT,EXPR):- is_dict(DICT,NAME),
    call(GEO,NAME,KEYS), NAME \= KEYS ,
    length(KEYS,LN), length(VALUES0,LN),
    maplist(key2DashAttr,KEYS,VALUES0,ATTRS),
    dict_create(DICTALL,NAME,ATTRS),
    DICT :< DICTALL, !,
    maplist(dictArg2Pl(CALL,GEO),ATTRS,EXPRS),
    EXPR =.. [NAME|EXPRS].
dict2Pl(CALL,GEO,DICT,EXPR):- is_dict(DICT,NAME), !,
    dict_pairs(DICT,NAME,PAIRS),
%    maplist(key2DashAttr,_KEYS,VALUES,PAIRS),
%    maplist(dict2pl(CALL,GEO),VALUES,LIST),
    maplist(dictArg2Pl(CALL,GEO),PAIRS,LIST),
    EXPR =.. [NAME|LIST].
dict2Pl(_CALL,_GEO,X,X).


dictArg2Pl(_CALL,_GEO,KEY-TRUE,KEY):- TRUE==true, !.
dictArg2Pl(CALL,GEO,KEY-VALUE,EXPR):- is_dict(VALUE,NAME), NAME=KEY, !,
    dict2Pl(CALL,GEO,VALUE,EXPR).
dictArg2Pl(CALL,GEO,_KEY-LIST,EXPRLIST):- is_list(LIST), !,
    maplist(dict2Pl(CALL,GEO),LIST,EXPRLIST).
dictArg2Pl(CALL,GEO,_-VALUE,EXPR):- dict2Pl(CALL,GEO,VALUE,EXPR).

:- begin_tests(plDict).

%:-run_tests(plDict:pl2dict).

%not dict args, but still are recognized as dicts
%pl2dict(VAR,X). ==> X=VAR
test(pl2dict):- pl2dict(VAR,X), var(X), X==VAR.
%pl2dict(12,X). ==> X=12
test(pl2dict):- pl2dict(12,X), X==12.
%pl2dict(al,X). ==> X=al
test(pl2dict):- pl2dict(a1,X), X==a1.
%pl2dict([12,34],X). ==> X=[12,34]
test(pl2dict):- pl2dict([12,34],X), X==[12,34].

%compounds are understood as dicts
%pl2dict(al(12),X). ==> X=al{12:true}
test(pl2dict):- pl2dict(al(12),X), X == al(12).
%pl2dict(a(ma),X). ==> X=a{ma:true}
test(pl2dict):- pl2dict(al(ma),X), X==al{ma:true}.
%pl2dict(a(A),X). ==> X = a(A)
test(pl2dict):- pl2dict(al(A),X), X=al(A).
%pl2dict(al(m,a),X). ==> X=al{m:true,a:true}.
test(pl2dict):- pl2dict(al(m,a),X), X==al{a:true,m:true}.
%pl2dict(al(ma([m,a]),X). ==> fail:a list can't be recognized
%                          as a pure dict arg
test(pl2dict):- pl2dict(al([m,a]),X), X=al{list:list{m:true,a:true}}.
%pl2dict(al(ma([12,34]),X). ==> X=al{ma:[12,34]}
test(pl2dict):- pl2dict(al(ma([12,34])),X), X==al{ma:[12,34]}.
%pl2dict(al(m(a),a(12),1:[c,d,e]),X) ==> X=al{m:a, a:12, 1:[c,d,e]})
test(pl2dict):- pl2dict(al(m(a),a(12),1:[c,d,e]),X),
    X==al{m:a, a:12, 1:[c,d,e]}.
%transformation with macro
%pl2dict(p2d,al(m(a),a(mama),b([c,d,e])),fel{a:mama, b:[c,d,e], m:a})
test(pl2dict):- pl2dict(plunit_plDict:p2d,=,
                        al(m(a),a(mama),b([c,d,e])), X),
                            X==fel{a:mama, b:[c,d,e], m:a}.

p2d(al(A,B,C),fel(A,B,C)).


dict(clause,[kind,free,subject,pred,num,pers,pol]).

%test cases
%there is a dictionary structure defined, one param is to be filled
%dict2pl(=,dict,clause{subject:sg(1)},X). X=clause(_,_,sg(1),_,_,_,_)
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg(1)},X),
    X=@=clause(_,_,sg(1),_,_,_,_).
%dict2pl(=,dict,clause{subject:sg/1},X). X=clause(_,_,sg/1,_,_,_,_)
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg/1},X),
    X=@=clause(_,_,sg/1,_,_,_,_).

%there is a dictionary structure defined, one param is to be filled
%dict2pl(clause{subject:sg(1)},X).
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg(1)},X),
    X=@=clause(_,_,sg(1),_,_,_,_).
%dict2pl(clause{subject:sg/1,pol: +},X).
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg/1,pol: +},X),
    X=@=clause(_,_,sg/1,_,_,_,+).
%dict2pl(clause{subject:sg(1),pol: +},X).
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg(1)},X),
    X=@=clause(_,_,sg(1),_,_,_,_).
% dict2pl(clause{subject:sg/1,pol: [+,-]},X). list of embedded atomics
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg/1,
                                                     pol:[alma,3,korte]},X),
    X=@=clause(_,_,sg/1,_,_,_,[alma,3,korte]).
% dict2pl(clause{subject:sg/1,pol: [+,-]},X). list with embedded dicts
test(dict2pl):-  dict2pl(=,plunit_plDict:dict,clause{subject:sg/1,
                                                     pol:[a{d: +},b{e: -}]},X),
    X=@=clause(_,_,sg/1,_,_,_,[a(+),b(-)]).


% there is a dictionary structure defined, embedded dict
% dict2pl(clause{subject:sg(1),pol: +},X).
test(dict2pl):- dict2pl(=,plunit_plDict:dict,
                        clause{subject:sg(1),pred:vp{a:b,c:d},pol: +},X),
    X=clause(_,_,sg(1),vp(b,d),_,_,+).

test(typeerror):- catch(dict2pl(alma,_X),error(E,_),true),
    E =@= type_error(dict,alma).

:- end_tests(plDict).
























