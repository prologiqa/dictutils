:-module(dictRe,[selectDict/3, unifyDicts/3
                 %,intersectDicts/5
                ]).

/** <module> - Prolog multiple level dict operations

               The module contains unification and intersection,
               as reimplementations of SWI-Prologs flat dict operations
               also for embedded dicts

 @author  Kili�n, Imre (Univ. P�cs, Hungary)
          kilian@gamma.ttk.pte.hu
 @license GPL

*/
:- use_module(library(plUnit)).


%! selectDict(+SELECT:dict,+FROM:dict,-REST:dict) is semidet.
%
% True when the tags of SELECT and FROM have been unified,
% all keys in SELECT appear in FROM and the corresponding values
% have been unified. The key-value pairs of FROM that
% do not appear in SELECT are used to form an anonymous dict,
% which us unified with REST.
%
% @arg SELECT the subdict of FROM
% @arg FROM the entire dict
% @arg REST remainder dict
%
% @error error(type_error(dict,_),_) is thrown
%        when either SELECT or FROM is not a Prolog dict


:- if(false).
:- redefine_system_predicate(select_dict(_,_,_)).

user:select_dict(SELECT,FROM,REST):- selectDict(SELECT,FROM,REST).

user:unify_dicts(DICT1,DICT2,UNIFIED):- unifyDicts(DICT1,DICT2,UNIFIED).

:- endif.

selectDict(SELECT,FROM,_):-
     \+ is_dict(SELECT), !, must_be(dict,SELECT);
     \+ is_dict(FROM), ! , must_be(dict,FROM).
selectDict(SELECT,FROM,REST):-
     selectdict(SELECT,FROM,REST).

selectdict(SELECT,FROM,REST):-
     dict_pairs(SELECT,TAG,SELECTLIST),
     dict_pairs(FROM,TAG,FROMLIST),
     selectDictlist(SELECTLIST,FROMLIST,RESTLIST),
     dict_pairs(REST,TAG,RESTLIST).

selectDictlist([],REST,REST):- !.
selectDictlist([KEY-SELECT|SELECTLIST],[KEY-FROM|FROMLIST],RESTLIST):-
    is_dict(SELECT), is_dict(FROM), !,
    selectdict(SELECT,FROM,REST),
    (REST=@=_{} -> selectDictlist(SELECTLIST,FROMLIST,RESTLIST);
     selectDictlist(SELECTLIST,FROMLIST,RESTL), RESTLIST=[KEY-REST|RESTL]).
selectDictlist([KEY-SELECT|SELECTLIST],[KEY-VALUE|FROMLIST],RESTLIST):-
    !, SELECT=VALUE,
    selectDictlist(SELECTLIST,FROMLIST,RESTLIST).
selectDictlist([KEYS-SELECT|SELECTLIST],[KEYF-FROM|FROMLIST],
               [KEYF-FROM|RESTLIST]):-
    KEYS @> KEYF, selectDictlist([KEYS-SELECT|SELECTLIST],FROMLIST,RESTLIST).


%! unifyDicts(+DICT1:dict,+DICT2:dict,-RESULT:dict) is semidet.
%
% True when the tags of DICT1 and DICT2 have been unified, and
% all keys that appear both dicts, can be unified
% unifying values mean unifying dicts recursively
% or Prolog-unifying values
%
% @arg DICT1 first dict to be unified
% @arg DICT2 second dict to be unified
% @arg RESULT unified dict
%
% @error error(type_error(dict,_),_) is thrown
%        when either DICT1 or DICT2 is not a Prolog dict

unifyDicts(DICT1,DICT2,_):-
     \+ is_dict(DICT1), !, must_be(dict,DICT1);
     \+ is_dict(DICT2), ! , must_be(dict,DICT2).
unifyDicts(DICT1,DICT2,RESULT):-
     unifydicts(DICT1,DICT2,RESULT).

unifydicts(DICT1,DICT2,RESULT):-
     dict_pairs(DICT1,TAG,DICTLIST1),
     dict_pairs(DICT2,TAG,DICTLIST2),
     unifyDictlist(DICTLIST1,DICTLIST2,RESULTLIST),
     dict_pairs(RESULT,TAG,RESULTLIST).

unifyDictlist(RESULT,[],RESULT):- !.
unifyDictlist([],RESULT,RESULT):- !.
unifyDictlist([KEY-VALUE1|DICTLIST1],[KEY-VALUE2|DICTLIST2],
              [KEY-RESULT|RESULTLIST]):-
    is_dict(VALUE1), is_dict(VALUE2), !,
    unifydicts(VALUE1,VALUE2,RESULT),
    unifyDictlist(DICTLIST1,DICTLIST2,RESULTLIST).
unifyDictlist([KEY-VALUE1|DICTLIST1],[KEY-VALUE2|DICTLIST2],
              [KEY-VALUE1|RESULTLIST]):-
    !, VALUE1=VALUE2,
    unifyDictlist(DICTLIST1,DICTLIST2,RESULTLIST).
unifyDictlist([KEY1-VALUE1|DICTLIST1],[KEY2-VALUE2|DICTLIST2],
               [KEY1-VALUE1|RESULTLIST]):-
    KEY2 @> KEY1, !, unifyDictlist(DICTLIST1,[KEY2-VALUE2|DICTLIST2],RESULTLIST).
unifyDictlist([KEY1-VALUE1|DICTLIST1],[KEY2-VALUE2|DICTLIST2],
               [KEY2-VALUE2|RESULTLIST]):-
    KEY1 @> KEY2, unifyDictlist([KEY1-VALUE1|DICTLIST1],DICTLIST2,RESULTLIST).




:- begin_tests(dictRe).

test(selectDict,[error(type_error(dict,alma),_)]):- selectDict(alma,a{},_Y).

test(selectDict,[error(type_error(dict,korte),_)]):- selectDict(a{},korte,_Y).

test(selectDict,[fail]):- selectDict(a{b:3},a{b:1,c:2},_).

test(selectDict,[fail]):- selectDict(b{c:3},a{b:1,c:2},_).

test(selectDict):- selectDict(a{b:X},a{b:1,c:2},R),
    X==1, R =@= a{c:2}.

test(selectDict):- selectDict(a{c:2},a{b:1,c:2},R),
    R =@= a{b:1}.

test(selectDict):- selectDict(a{c:X},a{b:1,c:d{e:f,g:h}},R),
    X == d{e:f,g:h}, R =@= a{b:1}.

test(selectDict):- selectDict(a{c:d{e:f}},a{b:1,c:d{e:f,g:h}},X),
    X == a{b:1,c:d{g:h}}.

test(unifyDicts,[error(type_error(dict,alma),_)]):- unifyDicts(alma,a{},_Y).

test(unifyDicts,[error(type_error(dict,korte),_)]):- unifyDicts(a{},korte,_Y).

test(unifyDicts,[fail]):- unifyDicts(a{b:3},a{b:1,c:2},_).

test(unifyDicts,[fail]):- unifyDicts(b{c:3},a{b:1,c:2},_).

test(unifyDicts):- unifyDicts(a{b:X},a{b:1,c:2},R),
    X==1, R =@= a{b:1,c:2}.

test(unifyDicts):- unifyDicts(a{c:2},a{b:1,c:2},R),
    R =@= a{b:1,c:2}.

test(unifyDicts):- unifyDicts(a{c:X},a{b:1,c:d{e:f,g:h}},R),
    X == d{e:f,g:h}, R =@= a{b:1,c:d{e:f,g:h}}.

test(unifyDicts):- unifyDicts(a{c:d{e:f}},a{b:1,c:d{e:f,g:h}},X),
    X == a{b:1,c:d{e:f,g:h}}.

:- end_tests(dictRe).














