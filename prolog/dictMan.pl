:-module(dictMan,[dictMatch/2, dictMatch/3
                  ,dictMatchEach/4
                  ,dictReplaceThis/4
                 ,dictReplaceEach/4, dictReplaceAll/4]).
/** <module> - Prolog expression to dict transformation and back

 The module exports predicates to search patterns in Prolog
 dicts and transform them to other dicts

 @author  Kili�n, Imre (Univ. P�cs, Hungary)
          kilian@gamma.ttk.pte.hu
 @license GPL

*/

:- use_module(dictRe).

:- use_module(library(plUnit)).

%! dictMatch(+DICT:dict,+PATTERN:dict) is semidet.
%! dictMatch(+DICT:dict,+PATTERN:dict,-REM:dict) is semidet.
%
% A complete dict matches a pattern, if the pattern is a complete
% subdict of the complete dict, that is the pattern occurs in the
% complete list, starting from the root, and if each of the subdicts in
% the pattern occur in the entire dict (pattern search by absolute
% addressing)
%
% @arg DICT the entire dict in which we are searching for PATTERN
% @arg PATTERN we are searching for PATTERN in DICT (must be a dict)
% @arg REM when PATTERN occurs in DICT, REM is the difference dict
%
% @error error(type_error(dict,_),_) is thrown
%        when either DICT or PATTERN is not a Prolog dictREPLACE=REPLACED

dictMatch(DICT,PATTERN):-
    \+ is_dict(DICT), !, must_be(dict,DICT);
    \+ is_dict(PATTERN), !, must_be(dict,PATTERN).
dictMatch(DICT,PATTERN):-
    dictMatch(DICT,PATTERN,_REM).

dictMatch(DICT,PATTERN,_REM):-
    \+ is_dict(DICT), !, must_be(dict,DICT);
    \+ is_dict(PATTERN), !, must_be(dict,PATTERN).
dictMatch(DICT,PATTERN,REM):-
    dictmatch(DICT,PATTERN,REM).

dictmatch(DICT,PATTERN,REM):-
    is_dict(PATTERN), !,
           dict_pairs(DICT,TAG,DICTLIST),
    dict_pairs(PATTERN,TAG,PATTERNLIST),
    pairsMatch(PATTERNLIST,DICTLIST,REMLIST,[]),
    dict_pairs(REM,TAG,REMLIST).

%! pairsMatch(+PATTERN:pairs,+DICT:pairs,-REM:pairs,-X:var) is nondet.
%
% A complete dict matches "each" a pattern if the pattern is a
% subdict of the entire dict, so that the pattern occurs
% starting from the root of the entire dict... (the predicate implements
% an absolute addressing starting from the root of the tree)
%
% @arg DICT    the entire dict in which we are searching for PATTERN
% @arg PATTERN we are searching for. It can be a dict or an expression
% @arg REM     the remaining dict (DICT-PATTERN),
%              when DICT contains PATTERN
% @arg VAR     the end-variable of REM
%
pairsMatch([],[],X,X):- ! .
pairsMatch([],[ITEM|VALUES0],[ITEM|VALUES],X):- !,
    pairsMatch([],VALUES0,VALUES,X) .
pairsMatch([KEY-PATTERN|PATTERNS],[KEY-VALUE|VALUES],REMS,X):-
%when keys (pattern;dict) are the same, and values are Prolog matching
    PATTERN=VALUE, !, pairsMatch(PATTERNS,VALUES,REMS,X).
pairsMatch([KEY-PATTERN|PATTERNS],[KEY-VALUE|VALUES],[KEY-REM|REMS],X):-
%when keys (pattern;dict) are the same, and values are both dicts
    is_dict(PATTERN), is_dict(VALUE), dictmatch(VALUE,PATTERN,REM),
    !, pairsMatch(PATTERNS,VALUES,REMS,X).
pairsMatch([KEYP-PATTERN|PATTERNS],[KEY-VALUE|VALUES],
           [KEY-VALUE|REMS],X):-
%when key of the pattern is greater than key of dict
    KEYP @> KEY, !,
    pairsMatch([KEYP-PATTERN|PATTERNS],VALUES,REMS,X).


% ! forEachSub(+DICT:dict,+PATTERN:dict,-SUB:dict,
%   -PATH:dict,-VAR:var) is nondet.
% list values are not resolved yet (thus fail)
forEachSub(DICT,DICT,X,X,X).
forEachSub(DICT,PATTERN,SUBDICT,PATH,VAR):- is_dict(DICT),
    dict_pairs(DICT,TAG,ATTRLIST), select(KEY-SUB,ATTRLIST,REMLIST),
    (fail, is_list(SUB)->member(MSUB,SUB),
      forEachSub(MSUB,PATTERN,SUBD,VALUE,VAR),
      dict_pairs(SUBDICT,TAG,[KEY-SUBD|REMLIST]),
      dict_pairs(PATH,TAG,[KEY-VALUE]);
    forEachSub(SUB,PATTERN,SUBD,VALUE,VAR),
    dict_pairs(SUBDICT,TAG,[KEY-SUBD|REMLIST]),
    dict_pairs(PATH,TAG,[KEY-VALUE])).

%!  forEachSub(+DICT:dict,+PATTERN:dict,-PATH:dict,-VAR:var) is nondet.
%   list values are not resolved yet (thus fail)
forEachSub(DICT,DICT,X,X).
forEachSub(DICT,PATTERN,PATH,VAR):- is_dict(DICT),
    dict_pairs(DICT,TAG,ATTRLIST), member(KEY-SUB,ATTRLIST),
    (fail, is_list(SUB)-> member(MSUB,SUB),
      forEachSub(MSUB,PATTERN,VALUE,VAR), dict_pairs(PATH,TAG,[KEY-VALUE]);
    forEachSub(SUB,PATTERN,VALUE,VAR), dict_pairs(PATH,TAG,[KEY-VALUE])).

%! dictMatchEach(+DICT:dict,+PATTERN:expr,-PATH:dict,-X:var) is nondet.
%
% A complete dict matches "each" a pattern if the pattern is a
% subdict of the entire dict, so that the pattern may occur anywhere in
% the entire dict... (the predicate implements a relative
% addressing starting from somewhere in the tree)
%
% @arg DICT    the entire dict in which we are searching for PATTERN
% @arg PATTERN we are searching for. It can be a dict or an expression
% @arg PATH    path, a subdict, leading to PATTERN in dict, ending
%              in a key selector, and having the value of VAR
% @arg REM     after a REM=PATTERN unification PATH will be
%              a complete subdict of DICT
%
% @error error(type_error(dict,_),_) is thrown
%        when either DICT or PATTERN is not a Prolog dict

dictMatchEach(DICT,PATTERN,PATH,REM):-
    forEachSub(DICT,SUB,PATH,REM),
      (is_dict(SUB), is_dict(PATTERN),
          dictMatch(SUB,PATTERN);
      SUB=PATTERN).

%! dictReplaceThis(+DICT,+PATTERN,+REPL,?REPLACED) is semidet.
%
% Search and replace operation in dicts. The pattern must exactly match
% (starting from root / absolute addressing)
%
% @arg DICT        input dict
% @arg PATTERN     dict pattern to match - or a plain expression
% @arg REPLACEMENT in case of match PATTERN should be replaced to this
% @arg REPLACED    resulting dict
%
% @error error(type_error(dict,_),_) is thrown
%        when either DICT or PATTERN is not a Prolog dict
%

dictReplaceThis(DICT,PATTERN,REPLACE,REPLACED):-
    \+ is_dict(DICT), !, must_be(dict,DICT);
    \+ is_dict(PATTERN), !, must_be(dict,PATTERN);
    dictReplacethis(DICT,PATTERN,REPLACE,REPLACED).


%if neither input nor the pattern are dicts, but they Prolog match
dictReplacethis(EXPR,EXPR,REPLACE,REPLACE):- \+ is_dict(EXPR), ! .
%if input and pattern are both dicts
dictReplacethis(DICT,PATTERN,REPLACE,REPLACED):-
    selectDict(PATTERN,DICT,REM), unifyDicts(REM,REPLACE,REPLACED).
/*
    dict_pairs(DICT,TAG,ATTRLIST0), is_dict(PATTERN), !,
    dict_pairs(PATTERN,TAG,PATTRLIST),
    dict_pairs(REPLACE,TAG,RATTRLIST),
    pairsReplaceThis(PATTRLIST,RATTRLIST,ATTRLIST0,ATTRLIST),
    dict_pairs(REPLACED,TAG,ATTRLIST).
*/

%!  pairsReplaceThis(+PATTERN:ordlist,+REPLACE:ordlist,
%!               +ATTRLIST0:ordlist,-ATTRLIST:ordlist) is semidet.
%
%   Each parameters are ordered lists, in which the original
%   ordering is not preserved.
%
%   The funktionality of the predicate: if the pattern matches,
%   starting from the root, than the pattern is replaced
%
% @arg PATTERN a list of KEY-VALUE pairs... pattern elements,
%              which each must occur in ATTRLIST
% @arg REPLACE if PATTERN occurs in ATTRLIST, then the occurences
%              are replaced to the elements of PATTERN
%              length(PATTERN)=length(REPLACE)
% @arg ATTRLIST0
% @arg ATTRLIST a pair of accumulators
%
pairsReplaceThis(PATTRS,REPLACEMENT,ATTRS0,ATTRS):-
    pairsMatch(PATTRS,ATTRS0,ATTRS,REMX), REMX=REPLACEMENT.


%! dictReplaceEach(+DICT,+PATTERN,+REPLACEMENT,?REPLACED) is nondet.
%
% Search and replace operation in dicts. The pattern must match
% any subdict of DICT (relative addressing)
% If it matches, the predicate returns one replacement
% nondeterministically
%
% @arg DICT        input dict
% @arg PATTERN     dict pattern to match - or a plain expression
% @arg REPLACEMENT in case of match, PATTERN should be replaced to this
% @arg REPLACED    resulting dict
%
% @error error(type_error(dict,_),_) is thrown
%        when either DICT or PATTERN is not a Prolog dict


dictReplaceEach(DICT,PATTERN,REPLACE,REPLACED):-
    \+ is_dict(DICT), !, must_be(dict,DICT);
    dictReplaceeach(DICT,PATTERN,REPLACE,REPLACED).


%if neither input nor the pattern are dicts, but they Prolog match
dictReplaceeach(EXPR,PATTERN,REPLACE,REPLACE):-
    \+ is_dict(EXPR), nonvar(EXPR), EXPR=PATTERN, ! .
%if input and pattern are both dicts, and the match absolutely
dictReplaceeach(DICT,PATTERN,REPLACE,REPLACED):-
    forEachSub(DICT,SUB,SUBDICT,_PATH,X),
      (is_dict(SUB), is_dict(PATTERN)->
          dictReplacethis(SUB,PATTERN,REPLACE,REPLACED);
      SUB=PATTERN, X=REPLACE, SUBDICT=REPLACED ).

%! dictReplaceAll(+DICT,+PATTERN,+REPL,?REPLACED) is semidet.
%
% Search and replace operation in dicts. The pattern must match
% all subdict of DICT (relative addressing)
% If any matches, the predicate returns the result, being
% all matches replaced to the replacement
%
% @arg DICT        input dict
% @arg PATTERN     dict pattern to match - or a plain expression
% @arg REPLACEMENT in case of match, PATTERN should be replaced to this
% @arg REPLACED    resulting dict
%
% @error error(type_error(dict,_),_) is thrown
%        when either DICT or PATTERN is not a Prolog dict
%

dictReplaceAll(DICT,PATTERN,REPLACE,REPLACED):-
    \+ is_dict(DICT)-> must_be(dict,DICT);
    dictReplaceall(PATTERN,REPLACE,DICT,REPLACED).


%if neither input nor the pattern are dicts, but they Prolog match
dictReplaceall(PATTERN,REPLACE,EXPR,REPLACED):-
    \+ is_dict(EXPR), nonvar(EXPR), PATTERN=EXPR, !,
    REPLACE=REPLACED.
%if both input and the pattern are dicts, and they Prolog match
dictReplaceall(PATTERN,REPLACE,EXPR,REPLACED):-
    is_dict(PATTERN), is_dict(EXPR), PATTERN=EXPR, !,
    REPLACE=REPLACED.
% if input is a list, then they elements are matched
dictReplaceall(PATTERN,REPLACE,LIST,REPLLIST):-
    is_list(LIST), !,
    maplist(dictReplaceall(PATTERN,REPLACE),LIST,REPLLIST).
%if both input and pattern and replace are dicts
%and pattern matches the input dict
dictReplaceall(PATTERN,REPLACE,DICT,REPLACED):-
    is_dict(DICT), is_dict(PATTERN), selectDict(PATTERN,DICT,REM0), !,
    dictReplaceall(PATTERN,REPLACE,REM0,REM),
    unifyDicts(REM,REPLACE,REPLACED).
%if input and pattern are both dicts, and the match absolutely
dictReplaceall(PATTERN,REPLACE,DICT,REPLACED):-
    is_dict(DICT), !, dict_pairs(DICT,TAG,ATTRLIST0),
    pairsReplaceAll(PATTERN,REPLACE,ATTRLIST0,ATTRLIST),
    dict_pairs(REPLACED,TAG,ATTRLIST).
%if neither input nor the pattern are dicts, but they Prolog match
dictReplaceall(_PATTERN,_REPLACE,EXPR,EXPR).
% if input is a list, then they elements are matched


pairsReplaceAll(_,_,[],[]):- ! .
pairsReplaceAll(PATTERN,REPLACE,
                [TAG-VALUE0|ATTRLIST0],[TAG-VALUE|ATTRLIST]):-
    dictReplaceall(PATTERN,REPLACE,VALUE0,VALUE), !,
    pairsReplaceAll(PATTERN,REPLACE,ATTRLIST0,ATTRLIST).
%pairsReplaceAll(PATTERN,REPLACE,
%                [TAG-VALUE|ATTRLIST0],[TAG-VALUE|ATTRLIST]):-
%    pairsReplaceAll(PATTERN,REPLACE,ATTRLIST0,ATTRLIST).


:- begin_tests(dictMan).

test(typeerror,[error(type_error(dict,alma),_)]):- dictMatch(alma,_X).

test(typeerror,[error(type_error(dict,korte),_)]):- dictMatch(a{b:c},korte).

test(match):- dictMatch(a{b:1,c:2},a{b:X},R),
    X==1, R =@= a{c:2}.

test(match,[fail]):- dictMatch(a{b:1,c:2},a{b:3},_).

test(match):- dictMatch(_{x:_{u:X,v:Y}}, _{x:_{u:4,v:5}},R),
    R =@= _{}, X==4, Y==5 .

test(match):- dictMatch( _{x:_T1{u:4,v:5},y:2,z:3},_{x:_T2{u:X,v:Y}},R),
    X==4, Y==5, R =@= _{y:2,z:3} .

test(match):- dictMatch( _{x:_{u:4,v:5,w:6},y:2,z:3},_{x:_T{u:_X,v:_Y}},R),
    R =@= _{x:_{w:6},y:2,z:3} .

test(foreach,all(X=[a{b:c{d:1,e:2},f:g},c{d:1,e:2},1,2,g])):-
    forEachSub(a{b:c{d:1,e:2},f:g},X,_PATH,_VAR).

test(foreach,all(PATH=[a{b:c{d:VAR}},a{b:c{e:VAR}},a{f:g{h:VAR}}])):-
    forEachSub(a{b:c{d:x,e:x},f:g{h:x}},x,PATH,VAR).

test(foreach,all(SUB=[a{b:c{d:VAR,e:x},f:g{h:x}},
                      a{b:c{d:x,e:VAR},f:g{h:x}},
                      a{b:c{d:x,e:x},f:g{h:VAR}}])):-
    forEachSub(a{b:c{d:x,e:x},f:g{h:x}},x,SUB,_PATH,VAR).

test(foreach,all(SUB=[VAR,a{b:VAR,f:g{h:VAR}},
                     a{b:c{d:VAR, e:x}, f:g{h:x}},
                     a{b:c{d:x, e:VAR}, f:g{h:x}},
                     a{b:c{d:x, e:x}, f:VAR},
                     a{b:c{d:x, e:x}, f:g{h:VAR}}])):-
    forEachSub(a{b:c{d:x,e:x},f:g{h:x}},_X,SUB,_PATH,VAR).

    %test(matchEach):-

test(replaceThis):-
    dictReplaceThis(a{b:c{d:1,e:2},f:g},a{f:g},a{h:15},X),
    X==a{b:c{d:1,e:2},h:15}.

test(replaceThis):-
    dictReplaceThis(a{b:c{d:1,e:2},f:g},a{b:c{d:1}},a{b:c{h:15}},X),
    X==a{b:c{e:2,h:15},f:g}.

test(replaceThis):-
    dictReplaceThis(a{b:c{d:1,e:2},f:g},a{f:G},a{h:15},X),
    X==a{b:c{d:1,e:2},h:15}, G==g.

test(replaceEach,all(X=[a{b:c{d:3,e:1+2},d:1+2},
                        a{b:c{d:1+2,e:3},d:1+2},
                        a{b:c{d:1+2,e:1+2},d:3}])):-
    dictReplaceEach(a{b:c{d:1+2,e:1+2},d:1+2},1+2,3,X).

%list values are not resolved yet... thus switched off
:- if(fail).
test(replaceEach,all(X=[a{b:c{d:[3,8,1+2],e:1+2},d:1+2},
                        a{b:c{d:[1+2,8,3],e:1+2},d:1+2},
                        a{b:c{d:[1+2,8,1+2],e:3},d:1+2},
                        a{b:c{d:[1+2,8,1+2],e:1+2},d:3}])):-
    dictReplaceEach(a{b:c{d:[1+2,8,1+2],e:1+2},d:1+2},1+2,3,X).
:-endif.

test(replaceAll):-
    dictReplaceAll(a{b:c{d:1+2,e:1+2},d:1+2},1+2,3,X),
    X==a{b:c{d:3,e:3},d:3}.

test(replaceAll):-
    dictReplaceAll(a{b:8,c:g{h:(1+2)},e:g{h:(1+2)}},
    g{h:(1+2)},gg{hh:3},X),
    X==a{b:8,c:gg{hh:3},e:gg{hh:3}}.

test(replaceAll):-
    dictReplaceAll(a{b:8,c:g{h:(1+2)},e:g{h:(1+2)}},
    1+2,2+1,X),
    X==a{b:8,c:g{h:(2+1)},e:g{h:(2+1)}}.

%if you find the pattern only once at the broadest dict
test(dictReplaceAll):-
    dictReplaceAll(a{b:8,c:g{h:(1+2)},e:g{h:(1+2)}},a{b:8},a{bb:88},X),
    X = a{bb:88, c:g{h:1+2}, e:g{h:1+2}}.

%if you find the pattern several times
test(dictReplaceAll):-
    dictReplaceAll(a{b:8,c:g{h:(1+2)},e:a{b:8}},a{b:8},a{bb:88},X),
    X = a{bb:88, c:g{h:1+2}, e:a{bb:88}}.

%Resolving multiple values (Prolog lists)
test(dictReplaceAll):-
    dictReplaceAll(a{b:8,c:g{h:[1+2,3,1+2]},e:a{b:8}},1+2,12,X),
    X = a{b:8, c:g{h:[12,3,12]}, e:a{b:8}}.

%if you dont find the pattern, replaceAll returns the dict unchanged
test(dictReplaceAll):-
    dictReplaceAll(a{b:8,c:g{h:(1+2)},e:g{h:(1+2)}},aa{b:8},a{bb:88},X),
    X=a{b:8,c:g{h:(1+2)},e:g{h:(1+2)}}.

%you can't change the main tag of a dict / neither by replaceAll
test(replaceAll,fail):-
    dictReplaceAll(a{b:8,c:g{h:(1+2)},e:g{h:(1+2)}},a{b:8},aa{bb:88},_X).

:- end_tests(dictMan).






























