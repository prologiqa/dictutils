
Package: dictUtils (dict utilities)

Author: Kili�n Imre (kilian@gamma.ttk.pte.hu)

Version: 1.0

Download: ????????


SWI-Prolog has introduced the datatype of dicts a few years ago.
The built-in dict handling package however lacks many utilities that
can be necessary for an efficient utilisation.

The presented package provides therefore the following possibilities:
- transformation from and to Prolog expressions. The transformation maps
  Prolog expression (positioned) arguments to labelled dictionary
  arguments, and it is controlled by an external data structure, called
  "feature geometry".
- pattern matching, also for embedded dicts
- search and replace possibility

The package has a built-in plunit test suite, that can also be used to find out
usage examples. The names of test suites are:
"plDict" for module pl2dict
"dictRe" for module dictRe
"dictMan" for module dictMan... 

Documentation can be generated using plDoc.


Installation:

:-pack_install('...full path of dictUtils-1.0.zip').


Contents of the package

README.txt			this file
LICENSE				Conditions for licensing (LGPL)
pack.pl				Package descriptor

prolog/pl2dict.pl		Prolog to/from dict conversion
prolog/dictRe.pl		Reimplementation of certain SWI dict handling
				procedures to work also for embedded lists
				(select, unify)
prolog/dictMan.pl  		Search and replace operations for dicts


